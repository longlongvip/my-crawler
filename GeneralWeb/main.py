# 爬虫必须库
import urllib.request
from bs4 import BeautifulSoup
import requests
from urllib.parse import urlencode

# 写Markdown
import markdown
import io
import csv
# 响应分析
import json

# 时间
import time
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36",
    "Cookie": "gr_user_id=4fa30551-0197-4fd8-b50a-1deb27756cad; SESSION=898e30c1-1b65-4fe1-a997-830e11369a93; b59341ccb802cc42_gr_session_id=3c15541a-20a4-45ad-ac19-cfd5bed0121c; b59341ccb802cc42_gr_session_id_3c15541a-20a4-45ad-ac19-cfd5bed0121c=true; acw_tc=2760822616158155094567475e379feedaf8a91535c500084b293106cd595d",
    "Referer": "https://www.epubit.com/books",
    "Accept": "application/json, text/plain, */*",
    "X-Requested": "XMLHttpRequest",
    "Connection": "keep-alive",
    "Host": "www.epubit.com",
    "Origin-Domain": "www.epubit.com"
}

base_url = "https://www.epubit.com/pubcloud/content/front/portal/getUbookList?"


def get_page(page, c=20):
    params = {
        'page': page,
        'row': 20,
        '': '',
        'startPrice': '',
        'endPrice': '',
        'tagId': ''
    }
    url = base_url + urlencode(params)
    print(url)
    # print("https://www.epubit.com/pubcloud/content/front/portal/getUbookList?page=1&row=20&=&startPrice=&endPrice=&tagId=")
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            # print(response.json())
            return response.json()
    except requests.ConnectionError as e:
        print('Error', e.args)


if __name__ == '__main__':
    with open("new.csv", 'a+', newline='', encoding="utf-8") as csv_file:
        writer = csv.writer(csv_file)
        for page in range(1, 77):
            json = get_page(page)
            results = json['data']['records']
            for result in results:
                # print(result['name'], result['authors'], result['price'], result['tagNames'])
                r = [result['name'], result['authors'], result['price'], result['tagNames']]
                writer.writerow(r)
            time.sleep(10)

